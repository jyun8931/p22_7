import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.File;
import java.io.FileInputStream;


public class WordCount extends Thread {

    private String filename;
    int countWord = 0;
    String line;

    public WordCount(String fname) {
        filename = fname;
    }

    @Override
    public void run() {
        try {
            File file = new File(filename);
            FileInputStream fileStream = new FileInputStream(file);
            InputStreamReader input = new InputStreamReader(fileStream);
            BufferedReader reader = new BufferedReader(input);

            while ((line = reader.readLine()) != null) {
                if (!(line.equals(""))) {
                    String[] wordList = line.split("\\s+");
                    countWord += wordList.length;
                }
            }
            System.out.println("" + filename + " : " + countWord);
            P22_7.totalCount += countWord;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("File " + filename + " not found.");
            System.out.println("" + filename + " : " + countWord);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("" + filename + " : " + countWord);
        }
    }
}
