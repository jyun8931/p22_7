/**
 * This is the main class that will run the thread
 * to count the words in a file.
 */

public class P22_7 {

    // static word count initialization
    static int totalCount = 0;
    // main method
    public static void main(String[] args) {
        WordCount w[] = new WordCount[args.length];

        try {
            if (args.length == 0) {
                throw new IllegalArgumentException();
            }
            else {
                for (int i = 0; i < args.length; i++) {
                    w[i] = new WordCount(args[i]);
                    w[i].run();
                }
                System.out.println("Joint count: " + totalCount);
            }
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.out.println("No args is passed in main function.");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
